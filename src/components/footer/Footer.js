// src/components/footer/Footer.js
import React from 'react';
import '../../assets/styles/Footer.css';

function Footer() {
  return (
    <footer className="bg-dark text-light p-4">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <p>Powered By Ing. Ramsen Orham</p>
          </div>
          <div className="col-md-4">
            <div className="footer-links facebook-icon">
              <a href="https://www.facebook.com/ramsen.orham" className="text-light">Facebook <em className="fab fa-facebook fa-lg"></em></a>
            </div>
          </div>
          <div className="col-md-4">
            <p className="mb-0">Impressum // Datenschutzerklärung</p>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
