// src/components/Header/Navigation.js
import React from 'react';
import {Link } from 'react-router-dom';
import '../../assets/styles/Navigation.css';

function Navigation() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">{/*navbar-dark bg-dark bg-light*/}
      <button className="navbar-toggler d-lg-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item active">
            <Link className="nav-link" to="/"><em className="fas fa-sticky-note"></em>PostIt</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/recipeSearch"><em className="fas fa-search"></em>RecipeSearch</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/shoppingList"><em className="fas fa-shopping-cart"></em>ShoppingList</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navigation;
