import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Alert, Modal  } from 'react-bootstrap';
import axios from 'axios';
//import '../../../assets/styles/RecipeSearch.css';

function RecipeSearch({ onSearchResults, searchResults }) {
  // State variables
  const [searchTerm, setSearchTerm] = useState('');
  const [recipes, setRecipes] = useState([]);
  const [error, setError] = useState('');
  const [selectedRecipe, setSelectedRecipe] = useState(null); // Adding the selected recipe for the modal

  useEffect(() => {
    if (searchResults.length > 0) {
      setRecipes(searchResults);
    }
  }, [searchResults]);

  // Function to handle the search for recipes
  const handleSearch = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?s=${searchTerm}`);
      const data = response.data;
      if (data.meals) {
        setRecipes(data.meals);
        setError('');
        onSearchResults(data.meals); // Update the search results in the parent component
      } else {
        setRecipes([]);
        setError('Keine Rezepte gefunden.');
        onSearchResults([]); // Update the search results in the parent component
      }
    } catch (error) {
      console.error(error);
      setError('Fehler beim Abrufen der Rezepte.');
    }
  };

  // Function to clear the search and reset state
  const handleClear = () => {
    setSearchTerm('');
    setRecipes([]);
    setError('');
    onSearchResults([]);
  };

  // Function to show a selected recipe in the modal
  const handleShowRecipe = (recipe) => {
    setSelectedRecipe(recipe);
  };

  // Function to close the recipe modal
  const handleCloseRecipe = () => {
    setSelectedRecipe(null);
  };

  // Return the JSX for the RecipeSearch component
  return (
    <div className="d-flex flex-column align-items-center">
      <Form onSubmit={handleSearch} className="mb-4">
        <Form.Group controlId="formSearchTerm">
          <Form.Control type="text" placeholder="Suchbegriff eingeben" value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} style={{ width: '300px' }} />
        </Form.Group>
        <Button variant="success" type="submit" style={{ width: '140px', marginRight: '10px' }}>
          Suche
        </Button>
        <Button variant="secondary" onClick={handleClear} style={{ width: '140px', marginLeft: '10px' }}>
          Leeren
        </Button>
      </Form>

      {error && <Alert variant="danger">{error}</Alert>}

      <div className="row">
        {recipes.map((recipe) => (
          <div key={recipe.idMeal} className="col-md-4 mb-4">
            <Card>
              <Card.Img variant="top" src={recipe.strMealThumb} />
              <Card.Body>
                <Card.Title>{recipe.strMeal}</Card.Title>
                <Card.Text>
                  <strong>Kategorie:</strong> {recipe.strCategory}
                </Card.Text>
                <Card.Text>
                  <strong>Tags:</strong> {recipe.strTags || 'Keine Tags'}
                </Card.Text>
                <Button variant="primary" href={recipe.strYoutube} target="_blank">
                  Video ansehen
                </Button>
                <Button variant="info" onClick={() => handleShowRecipe(recipe)}> {/* Button für das Modal */}
                  Rezept anzeigen
                </Button>
              </Card.Body>
            </Card>
          </div>
        ))}
      </div>
      {/* Modal for displaying recipe information */}
      <Modal show={selectedRecipe !== null} onHide={handleCloseRecipe}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedRecipe && selectedRecipe.strMeal}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* Display recipe information here */}
          <p><strong>Zutaten:</strong> {selectedRecipe && selectedRecipe.strIngredient1}, {selectedRecipe && selectedRecipe.strIngredient2}, {selectedRecipe && selectedRecipe.strIngredient3}</p>
          <p><strong>Anweisungen:</strong> {selectedRecipe && selectedRecipe.strInstructions}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseRecipe}>
            Schließen
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default RecipeSearch;
