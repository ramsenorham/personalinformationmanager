import Quantity from "./Quantity";
import TextEdit from "./TextEdit";
import Unit from "./Unit";
import "../../../assets/styles/ShoppingList.css";

const ShoppingItems = (props) => {

    const shoppingItems = props.listData.map((item, index) => (
        props.showCompleteteItems || (!props.showCompleteteItems && !props.itemState[index].checked) ? ( 
            <li key={item.id}
                    title={item.information}
                    className={"grid my-subgrid col-start-1 col-end-6 ml-2" + (props.sortByCategory ? ' order-' + (props.categories.indexOf(item.category) + 1) : '') }>
                    {!props.itemState[index].checked ? (
                            <span className="material-symbols-outlined text-gray-700 text-base"
                                onClick={() => props.changeItemState(index)} >
                                check_box_outline_blank
                            </span> ) : (
                            <span className="material-symbols-outlined text-gray-400  text-base"
                                onClick={() => props.changeItemState(index)} >
                                check_box
                            </span> )
                    }
                    <Quantity
                        quantity={item.quantity}
                        updateData={props.updateData}
                        itemIndex={index}
                        itemChecked={props.itemState[index].checked} />

                    <Unit
                        unit={item.unit}
                        updateData={props.updateData}
                        itemIndex={index}
                        itemChecked={props.itemState[index].checked} />
                    <TextEdit
                        content={item.title}
                        updateData={props.updateData}
                        itemIndex={index}
                        itemState={props.itemState}
                        itemChecked={props.itemState[index].checked} />
                    <div className="text-right w-10">
                        <span className="material-symbols-outlined text-lg text-black hover:text-rose-700"
                            title="Eintrag Löschen"
                            lang="en"
                            onClick={() => props.handleDelete(index)} >
                            delete
                        </span>
                    </div>
                </li> ) : ''))
                return (
                <>
                    {shoppingItems}
                </>
    );
}

export default ShoppingItems;


