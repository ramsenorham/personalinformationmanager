import { Fragment } from "react";

const UnitActive = ({ unit, handleChange }) => {
    const units = ['l', 'g', 'kg', 'St.', 'Pk.'];

    return (
        <div className={"text-left relative"}>
            <select className={"w-10 absolute left-0"}
                autoFocus
                defaultValue={unit}
                onChange={handleChange}
                onBlur={handleChange} >
                {units.map((el, index) => (
                    <Fragment key={index}>
                        {el === unit ? (
                            <>
                                <option style={{ display: 'none' }} value={el} > {el} </option>
                                <option value={'same'} > {el} </option>
                            </> ) : (
                                <option value={el} > {el} </option>
                            )
                        }
                    </Fragment>
                ))
                }
            </select>
        </div>
    );
}

export default UnitActive;