import { useState } from "react";

const TextEdit = (props) => {

    const [content, setContent] = useState(props.content);

    const handleChange = (ev) => {
        const newContent = ev.target.textContent;
        if (newContent !== content) {
            setContent(newContent)
            props.updateData('title', newContent, props.itemIndex);
        }
    }

    const componentInActive = props.itemChecked;

    return (
        <div
            className={"w-40 max-w-[160px] overflow-hidden text-ellipsis" + (!props.itemChecked ? '' : ' line-through text-gray-400')}
            contentEditable={!componentInActive ? true : null}
            suppressContentEditableWarning={true}
            onBlur={handleChange}
            title={content} >
            {content}
        </div>
    );
}

export default TextEdit;