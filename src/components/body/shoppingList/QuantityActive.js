const QuantityActive = ({ quantity, handleChange, handleEnterKeyPressed, handleBlur }) => {
    return (
        <div className={"text-right relative"} >
            <input  className={"text-right w-8 absolute right-0"}
                type="text"
                value={quantity}
                autoFocus
                maxLength={6}
                onChange={handleChange}
                onKeyDown={handleEnterKeyPressed}
                onBlur={handleBlur} />
        </div>
    );
}

export default QuantityActive;