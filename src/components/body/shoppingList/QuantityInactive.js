const QuantityInactive = ({ quantity, handleChange, itemChecked }) => {

    return (
        <div className={"text-right" + (!itemChecked ? '' : ' line-through text-gray-400')} onClick={handleChange} >
            {quantity}
        </div>
    );
}

export default QuantityInactive;