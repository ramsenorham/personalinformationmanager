const UnitInactive = ({ unit, handleChange, itemChecked }) => {
    return (
        <div className={"text-left"  + (!itemChecked ? '' : ' line-through text-gray-400')} onClick={handleChange} >
            {unit}
        </div>
    );
}

export default UnitInactive;