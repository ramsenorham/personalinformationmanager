const NewItem = (props) => {

    const units = ['l', 'g', 'kg', 'St.', 'Pk.'];
    const categories = props.categories;

    return (
        <form className="max-w-[768px] min-w-[640px] w-[50vw] mx-auto" onSubmit={props.submitHandler} >
            <fieldset className="my-5">
                <legend className=" mb-8 pb-4 w-full text-2xl font-bold tracking-tight text-sky-700 dark:text-white border-b border-solid border-gray-200">
                    Neuer Eintrag
                </legend>
                <p>
                    <label className="block mb-2 text-sm font-semibold text-sky-700 dark:text-white" htmlFor="quantity" >Menge</label>
                    <input className="bg-gray-50 border border-gray-300 text-sky-700 text-sm rounded-lg focus:ring-sky-700 focus:border-sky-700 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-sky-700 dark:focus:border-sky-700"
                        type="text"
                        id="quantity"
                        name="quantity"
                        value={props.data.quantity}
                        onChange={props.handleChange}
                        style={props.invalidFormEl.includes("quantity") ? { outline: "2px solid tomato" } : null} />
                </p>
                <p className="mt-4">
                    <label className="block mb-2 text-sm font-semibold text-sky-700 dark:text-white" htmlFor="unit" >Einheit</label>
                    <select className="bg-gray-50 border border-gray-300 text-sky-900 text-sm rounded-lg focus:ring-sky-700 focus:border-sky-700 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-sky-700 dark:focus:border-sky-700"
                        id="unit"
                        name="unit"
                        value={props.data.unit}
                        onChange={props.handleChange}
                        style={props.invalidFormEl.includes("unit") ? { outline: "2px solid tomato" } : null} >
                        <option value="">--bitte auswählen--</option>
                        {units.map((el, index) => (
                            <option key={index} value={el} >
                                {el}
                            </option>
                        ))
                        }
                    </select>
                </p>
                <p className="mt-4">
                    <label className="block mb-2 text-sm font-semibold text-sky-700 dark:text-white" htmlFor="title" >Titel</label>
                    <input className="bg-gray-50 border border-gray-300 text-sky-700 text-sm rounded-lg focus:ring-sky-700 focus:border-sky-700 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-sky-700 dark:focus:border-sky-700"
                        type="text"
                        id="title"
                        name="title"
                        value={props.data.title}
                        onChange={props.handleChange}
                        style={props.invalidFormEl.includes("title") ? { outline: "2px solid tomato" } : null} />
                </p>
                <p className="mt-4">
                    <label className="block mb-2 text-sm font-semibold text-sky-700 dark:text-white" htmlFor="information" >Zusatzinformation</label>
                    <input className="bg-gray-50 border border-gray-300 text-sky-700 text-sm rounded-lg focus:ring-sky-700 focus:border-sky-700 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-sky-700 dark:focus:border-sky-700"
                        type="text"
                        id="information"
                        name="information"
                        value={props.data.information}
                        onChange={props.handleChange}
                        style={{outlineWidth: "1px"}} />
                </p>
                <p className="mt-4">
                    <label className="block mb-2 text-sm font-semibold text-sky-700 dark:text-white" htmlFor="category" >Kategorie</label>
                    <select className="bg-gray-50 border border-gray-300 text-sky-900 text-sm rounded-lg focus:ring-sky-700 focus:border-sky-700 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-sky-700 dark:focus:border-sky-700"
                        id="category"
                        name="category"
                        value={props.data.category}
                        onChange={props.handleChange}
                        style={props.invalidFormEl.includes("category") ? { outline: "2px solid tomato" } : null} >
                        <option value="">--bitte auswählen--</option>
                        {categories.map((el, index) => (
                            <option key={index} value={el} >
                                {el}
                            </option>
                        ))
                        }
                    </select>
                </p>
            </fieldset>
            <fieldset className="mb-5 mt-8 pt-8 border-t border-solid border-gray-200">
                <button className="mr-4 px-3 py-2 text-xs font-semibold text-center text-white bg-emerald-700 rounded-lg hover:bg-emerald-800 focus:outline-1 focus:outline-dotted focus:outline-amber-700 active:bg-gray-700 dark:bg-emerald-600 dark:hover:bg-emerald-700 dark:focus:outline-amber-700"
                    type="submit">
                        Hinzufügen
                </button>
                <button className="px-3 py-2 text-xs font-semibold text-center text-white bg-rose-700 rounded-lg hover:bg-rose-800 focus:outline-1 focus:outline-dotted focus:outline-amber-700 active:bg-gray-700 dark:bg-rose-600 dark:hover:bg-rose-700 dark:focus:outline-amber-700"
                    onClick={props.handleAbort} >
                        Abbrechen
                </button>
            </fieldset>
        </form>
    );
}

export default NewItem;