import { useReducer, useRef, useState } from "react";
import ShoppingItems from "./ShoppingItems";
import NewItem from "./NewItem";
import "../../../assets/styles/dialog.css";

const reducer = (state, action) => {
    switch (action.type) {
        case "ADD_DATA":
            return {
                ...state,
                data: state.data.concat(action.newEntryPayLoad)
            }
        case "DELETE_DATA":
            state.data.splice(action.index, 1);
            const newData = state.data.slice(0);
            return {
                ...state,
                data: newData,
            }
        case "UPDATE_DATA":
            state.data[action.index] = {
                ...state.data[action.index],
                [action.key]: action.value,
            }
            const changedData = state.data.slice(0);
            return {
                ...state,
                data: changedData,
            }
        default:
            break;
    }
}

const ShoppingList = ({ listData, handleChangeData }) => {
    const initialState = {
        data: listData,
    }

    const initalNewEntry = {
        "id": "",
        "quantity": "",
        "unit": "",
        "title": "",
        "information": "",
        "category": "",
    };

    const categories = ['food', 'conviniece', 'non food'];

    const [state, dispatch] = useReducer(reducer, initialState);
    const [newEntry, setNewEntry] = useState(initalNewEntry);
    const [showCompleteteItems, setShowCompleteteItems] = useState(true);
    const [sortByCategory, setSortByCategory] = useState(false);
    const [invalidFormEl, setInvalidFormEl] = useState([]);

    const initialItemState = listData.map((item) => ({
        id: item.id,
        checked: false,
    }))

    const [itemState, setItemState] = useState(initialItemState);

    const changeItemState = (index) => {
        setItemState(itemState.map(
            (el, idx) => (idx === index)
                ? ({ id: el.id, checked: !el.checked })
                : el
        ));
    }

    const dialogElement = useRef();

    const localDate = new Date().toLocaleString('de-DE').split(',')[0];

    const deleteData = (index) => {
        // Eintrag aus dem ItemState entfernen
        setItemState(itemState.filter((el, idx) => (idx !== index)));
        dispatch({ type: "DELETE_DATA", index });
        // Aktualisierung der Daten in Elternkomponent - simuliert Datenbanksynchronisation
        handleChangeData(state.data);
    }


    const updateData = (key, value, index) => {
        dispatch({ type: "UPDATE_DATA", key, value, index })
        // Aktualisierung der Daten in Elternkomponent - simuliert Datenbanksynchronisation
        handleChangeData(state.data);
    }

    const addData = () => {
        // id muss hinzugefügt werden, wir haben keine Datenbankverbindung für das insert
        const newId = state.data[state.data.length - 1].id + 1
        const newEntryPayLoad = {
            ...newEntry,
            id: newId
        }

        // neuen Eintrag mit in den ItemState aufnehmen
        setItemState([
            ...itemState, 
            { id: newId, checked: false }
        ]);

        // newEntry zurücksetzen, damit das Formular beim nächsten Aufruf leer ist
        setNewEntry(initalNewEntry);

        // invalidFormEl zurücksetzen,
        setInvalidFormEl([]);

        dispatch({ type: "ADD_DATA", newEntryPayLoad })
        // Aktualisierung der Daten in Elternkomponent - simuliert Datenbanksynchronisation
        handleChangeData(state.data);
    }

    // Modal-Aktionen

    const openModal = () => {
        dialogElement.current.showModal();
    }

    const closeModal = (ev = false) => {
        // event stoppen, sonst erfolgt eine erneute Validierung und die invalidFormEl ist wieder gefüllt
        if (ev) ev.preventDefault();
        setNewEntry(initalNewEntry);
        setInvalidFormEl([]);
        dialogElement.current.close();
    }

    const addToNewEntry = (ev) => {
        // Eingabe überprüfen
        const [key, value] = validateData(ev.target.name, ev.target.value);
        setNewEntry({
            ...newEntry,
            [key]: value,
        });
    }

    // Validierung Formulardaten

    const validateData = (key, value) => {
        switch (key) {
            case "quantity":
                value = (/^-?[0-9]*$/.test(value) && value >= 1 && value <= 1000) ? value : newEntry.quantity;
                break;
            // hier ggf. weitere cases
            default:
                break;
        }
        return [key, value];
    }

    const validateNewEntry = (ev) => {
        ev.preventDefault();
        // Prüfen ob alle Pflichtangaben gemacht wurden
        const requiredValues = ["quantity", "unit", "title", "category",];
        if (requiredValues.every(key => key in newEntry && newEntry[key] !== '')) {
            const newId = state.data.reduce((acc, el) => (el.id > acc) ? el.id : acc, 0) + 1;
            setNewEntry({ ...newEntry, "id": newId });
            setInvalidFormEl([]);
            addData();
            closeModal();
        } else {
            setInvalidFormEl(requiredValues.map(key => (newEntry[key] === '') ? key : null));
        };
    }

    // Filter
    const toggleShowCompleteteItems = () => {
        setShowCompleteteItems(!showCompleteteItems);
    }

    const toggleSortByCategory = () => {
        setSortByCategory(!sortByCategory);
    }

    return (
        <section>
            <h2 className="flex justify-center items-center mb-4 pb-2 text-2xl font-bold tracking-tight text-black border-b border-solid border-black dark:text-white">
                Einkaufsliste <span className="text-lg text-slate-400 text-2xl text-black">&nbsp; &nbsp; &nbsp;{localDate}</span>
            </h2>
            <div className="justify-center">
                {!sortByCategory ? (
                        <span className="material-symbols-outlined text-black hover:emerald-700"
                            title="nach Kategorien sortieren"
                            onClick={toggleSortByCategory} >
                            list
                        </span> ) : (
                        <span className="material-symbols-outlined text-black hover:emerald-700"
                            title="Erledigte Einträge einblenden"
                            onClick={toggleSortByCategory} >
                            segment
                        </span> )
                }
                {showCompleteteItems ? (
                        <span className="material-symbols-outlined text-black hover:emerald-700"
                            title="Erledigte Einträge ausblenden"
                            onClick={toggleShowCompleteteItems} >
                            visibility
                        </span> ) : (
                        <span className="material-symbols-outlined text-black hover:emerald-700"
                            title="Erledigte Einträge einblenden"
                            onClick={toggleShowCompleteteItems} >
                            visibility_off
                        </span> )
                }
            </div>
            <ul className="grid grid-flow-col auto-cols-max auto-rows-auto gap-x-1 mb-4 justify-center">
                {sortByCategory ? (
                        <>
                            <li className="col-start-1 col-end-6 order-1 text-lg text-sky-700 font-medium first:mt-0 mt-4">
                                <span className="material-symbols-outlined text-black">
                                    grocery
                                </span>&nbsp;
                                <span className="align-super text-black">Food</span>
                            </li>
                            <li className="col-start-1 col-end-6 order-2 text-lg text-sky-700 font-medium first:mt-0 mt-4"><span className="material-symbols-outlined text-black">
                                set_meal
                            </span>&nbsp;
                                <span className="align-super text-black">Convenience-Food</span>
                            </li>
                            <li className="col-start-1 col-end-6 order-3 text-lg text-sky-700 font-medium first:mt-0 mt-4">
                                <span className="material-symbols-outlined text-black">
                                    no_food
                                </span>&nbsp;
                                <span className="align-super text-black">Non-Food</span>
                            </li>
                        </> ) : (
                        <li className="col-start-1 col-end-6 text-lg text-sky-700 font-medium first:mt-0 mt-4">
                            <span className="material-symbols-outlined text-black">
                                list_alt
                            </span>&nbsp;
                            <span className="align-super text-black">unsortiert</span>
                        </li> )
                }
                <ShoppingItems
                    listData={state.data}
                    categories={categories}
                    showCompleteteItems={showCompleteteItems}
                    sortByCategory={sortByCategory}
                    updateData={updateData}
                    handleDelete={deleteData}
                    itemState={itemState}
                    changeItemState={changeItemState} />
            </ul>
            <hr />
            <div className="pl-8 text-center">
                <button className="px-3 py-2 font-medium text-center text-white bg-emerald-700 rounded-lg hover:bg-emerald-800 focus:outline-1 focus:outline-dotted focus:outline-amber-700 active:bg-gray-700 dark:bg-emerald-600 dark:hover:bg-emerald-700 dark:focus:outline-amber-700"
                    onClick={openModal}>
                    neuer Eintrag
                </button>
            </div>
            <dialog className="overflow-y-auto overflow-x-hidden justify-center items-center w-fit md:inset-0 bg-amber-50 px-8 py-4" ref={dialogElement}>
                <NewItem
                    submitHandler={validateNewEntry}
                    data={newEntry}
                    categories={categories}
                    handleChange={addToNewEntry}
                    handleAbort={closeModal}
                    invalidFormEl={invalidFormEl} />
            </dialog>
        </section>
    );
}

export default ShoppingList;