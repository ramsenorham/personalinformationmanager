const DataError = (props) => {
    return (
        <article className="block mt-8 mx-auto max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <h2 className="text-xl text-red-700 whitespace-nowrap font-bold mb-8" >
                {props.message ?? 'Fehler bei Datenbankverbindung!'}
            </h2>
            <div className="w-auto text-center" role="status">
                <span className="material-symbols-outlined text-6xl text-red-700">
                    {props.icon}
                </span>
            </div>
        </article>
    );
}

export default DataError;
