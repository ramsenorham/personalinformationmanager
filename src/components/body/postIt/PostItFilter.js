// 'src/components/body/PostItFilter.js'
import React from 'react';

function PostItFilter({ onFilterChange, onSortChange }) {
  const handleFilterClick = (filterType) => {
    // Hier wird der ausgewählte Filtertyp an die übergeordnete Komponente übergeben
    onFilterChange(filterType);
  };

  const handleSortClick = () => {
    // Hier könnte die Logik für die Sortierung implementiert werden
    onSortChange();
  };

  return (
    <div className="mb-4">
      <h4><em className="fas fa-filter"></em>Filter</h4>
      <div className="btn-group-vertical" role="group">
        <button onClick={() => handleFilterClick('all')} className="btn btn-secondary">
          Alle
        </button>
        <button onClick={() => handleFilterClick('active')} className="btn btn-primary">
          Offene
        </button>
        <button onClick={() => handleFilterClick('completed')} className="btn btn-success">
          Erledigte
        </button>
      </div>
      <div>
        <button onClick={() => handleSortClick()} className="btn btn-info">
          Sortieren
        </button>
      </div>
    </div>
  );
}

export default PostItFilter;
