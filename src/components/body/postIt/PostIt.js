// 'src/components/body/PostIt.js'
import React, { useState, useReducer } from 'react';
import ModalForm from './ModalForm'; // Import the ModalForm component
import PostItFilter from './PostItFilter';
import '../../../assets/styles/PostIt.css'; // Stile für die PostIt-Komponente
import postItsData from '../../../utils/data.json';

// Reducer function to manage state changes for post-its
const postItReducer = (state, action) => {
  switch (action.type) {
    // Add a new post-it to the state
    case 'ADD':
      return [...state, action.payload];
    // Delete post-its from the state
    case 'DELETE':
      return state.filter(postIt => !action.payload.includes(postIt.id));
    // Update post-its in the state
    case 'UPDATE':
      return state.map(postIt => {
        if (action.payload.ids.includes(postIt.id)) {
          return {
            ...postIt,
            ...action.payload.update
          };
        }
        return postIt;
      });
    default:
      return state;
  }
};

function PostIt() {
  // State variables
  const [isModalOpen, setModalOpen] = useState(false); // Add a new state variable for the modal form
  const [filterType, setFilterType] = useState('all'); //active, completed
  const [sortDirection, setSortDirection] = useState('asc'); // 'asc' für aufsteigend, 'desc' für absteigend
  const [state, dispatch] = useReducer(postItReducer, postItsData);
  const [selectedPostIts, setSelectedPostIts] = useState([]);

  // Function to toggle the modal form
  const toggleModal = () => {
    setModalOpen(!isModalOpen);
  };

  const handleFilterChange = (newFilterType) => {
    setFilterType(newFilterType);
  };

  const handleSortChange = () => {
    const newSortDirection = sortDirection === 'asc' ? 'desc' : 'asc';
    setSortDirection(newSortDirection);
  };

  const applyFiltersAndSort = () => {
    // Filtern basierend auf dem ausgewählten Filtertyp
    let filteredPostIts = [...state];

    if (filterType === 'active') {
      filteredPostIts = filteredPostIts.filter((postIt) => !postIt.completed);
    } else if (filterType === 'completed') {
      filteredPostIts = filteredPostIts.filter((postIt) => postIt.completed);
    }

    // Sorting based on the selected sort direction
    const sortedFilteredPostIts = [...filteredPostIts].sort((a, b) => {
      const dateA = new Date(a.createdDate);
      const dateB = new Date(b.createdDate);

      if (sortDirection === 'asc') {
        return dateA - dateB;
      } else {
        return dateB - dateA;
      }
    });

    return sortedFilteredPostIts;
  };

  // Function to handle adding a new post-it
  const handleAddPostIt = (newPostIt) => {
    // Add the new post-it to the list
    const currentDate = new Date().toLocaleDateString(); // Current date as a string
    dispatch({ type: 'ADD', payload: { id: state.length + 8, ...newPostIt, createdDate: currentDate, modifiedDate: '', completed: false } });
  };

  const handleDeletePostIts = () => {
    dispatch({ type: 'DELETE', payload: selectedPostIts });
    setSelectedPostIts([]);
  };

  const handlePostItSelection = (postId) => {
    // Aktualisiere die ausgewählten PostIts
    if (selectedPostIts.includes(postId)) {
      setSelectedPostIts(selectedPostIts.filter(id => id !== postId));
    } else {
      setSelectedPostIts([...selectedPostIts, postId]);
    }
  };

  // Function to mark post-its as completed
  const handleMarkCompleted = () => {
    const currentDate = new Date().toLocaleDateString();
    // Set completed to true
    dispatch({
      type: 'UPDATE',
      payload: {
        ids: selectedPostIts,
        update: { modifiedDate: currentDate, completed: true }
      }
    });
  };

  // Apply filters and sort the post-its
  const filteredPostIts = applyFiltersAndSort();

  // Return the JSX for the PostIt component
  return (
    <section id="PostIt" className="container mt-4">
      <div className="row">
        <div className="col-md-3">
          {/* Linke Seite mit AddPostIt und PostItFilter */}
          <div>
            <h4><em className="fas fa-sticky-note"></em>PostIt</h4>
            <button onClick={toggleModal} className="btn btn-danger mb-2">
              {isModalOpen ? 'Cancel' : 'Add PostIt'}
            </button>
            {isModalOpen && <ModalForm onAddPostIt={handleAddPostIt} onCloseModal={toggleModal} />}
          </div>
          <div>
            <PostItFilter onFilterChange={handleFilterChange} onSortChange={handleSortChange} />
          </div>
          {selectedPostIts.length > 0 && (
            <div className="btn-group-vertical" role="group">
              <button onClick={handleMarkCompleted} className="btn btn-success">Completed</button>
              <button onClick={handleDeletePostIts} className="btn btn-danger">Delete</button>
            </div>
          )}
        </div>
        <div className="col-md-9">
          {/* Rechte Seite mit den PostIt-Karten */}
          <div className="row">
            {filteredPostIts.map(postIt => (
              <div key={postIt.id} className="col-md-4 mb-4">
                <div className={`card h-100 ${postIt.completed ? 'completed' : 'active'}`}>
                  <div className="card-body">
                    <div className="mr-3 d-flex">
                      <input type="checkbox" checked={selectedPostIts.includes(postIt.id)} onChange={() => handlePostItSelection(postIt.id)} />  
                    </div>
                    <div className="text-center">
                      <h4 className="card-title">
                        {postIt.title}
                      </h4>
                    </div>
                    <div>
                      <p className="card-text">{postIt.description}</p>
                      <p className="card-text">Erstellt am: {postIt.createdDate}</p>
                      <p className="card-text">Geändert am: {postIt.modifiedDate}</p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
}

export default PostIt;
