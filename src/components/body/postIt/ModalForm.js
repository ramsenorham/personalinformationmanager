import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

function ModalForm({ onAddPostIt, onCloseModal }) {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Perform validation
    if (title.trim() === '' || description.trim() === '') {
      alert('Bitte füllen Sie alle Felder aus');
      return;
    }
    // Call the onAddPostIt function with the new postIt object
    onAddPostIt({ title, description });
    // Reset the form fields
    setTitle('');
    setDescription('');
    // Close the modal
    onCloseModal();
  };

  return (
    <Modal show={true} onHide={onCloseModal} centered>
      <Modal.Header closeButton>
        <Modal.Title>Add PostIt</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="formTitle">
            <Form.Label>Title</Form.Label>
            <Form.Control type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
          </Form.Group>
          <Form.Group controlId="formDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control as="textarea" rows={3} value={description} onChange={(e) => setDescription(e.target.value)} />
          </Form.Group>
          <Button variant="primary" type="submit" className="mx-auto d-block">
            Add
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}

export default ModalForm;
