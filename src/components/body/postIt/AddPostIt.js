// 'src/components/body/AddPostIt.js'
import React, { useState } from 'react';

function AddPostIt({ onAddPostIt }) {
  const [newPostIt, setNewPostIt] = useState({
    title: '',
    description: '',
  });

  const handleChange = (e) => {
    setNewPostIt({
      ...newPostIt,
      [e.target.name]: e.target.value,
    });
  };

  const handleAddClick = () => {
    // Füge hier die Logik hinzu, um das neue PostIt hinzuzufügen
    onAddPostIt(newPostIt);
    // Setze das State für neue PostIt zurück
    setNewPostIt({
      title: '',
      description: '',
    });
  };

  return (
    <div className="mb-4">
      <h4>Add PostIt</h4>
      <div className="form-group">
        <label>Title:</label>
        <input type="text" name="title" value={newPostIt.title} onChange={handleChange} className="form-control" />
      </div>
      <div className="form-group">
        <label>Description:</label>
        <textarea name="description" value={newPostIt.description} onChange={handleChange} className="form-control" />
      </div>
      <button onClick={handleAddClick} className="btn btn-primary">
        Add PostIt
      </button>
    </div>
  );
}

export default AddPostIt;
