// src/components/Body/Body.js
import React, { useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import PostIt from './postIt/PostIt';
import RecipeSearch from './recipeSearch/RecipeSearch';
import ShoppingList from './shoppingList/ShoppingList';
import {fakeData as listData} from '../../utils/fakeData';

function Body() {
  //React-Hook useState, um den Zustand der searchResults-Variable zu verwalten
  const [searchResults, setSearchResults] = useState([]);

  //um die Suchergebnisse zu aktualisieren
  const handleSearchResults = (results) => {
    setSearchResults(results);
  };

  const updateStateData = (changedData) => {
    // Aktualisierung der Statevariablen data - alle CRUD-Operationen werden synchronisiert
    console.log("updateStateData...");
  };

  return (
    <Routes>
      {/* Hier können Sie Ihre Navigation einfügen: je nach Navi wird bestimmte Seite hier in Body angezeigt: PostIt, RecipeSearch oder ShoppingList Seite */}
      <Route path="/" element={<PostIt />} />
      <Route path="/recipeSearch" element={<RecipeSearch onSearchResults={handleSearchResults} searchResults={searchResults}/>} />
      <Route path="/shoppingList" element={<ShoppingList listData={listData} handleChangeData={updateStateData}/>} />
    </Routes>
  );
}

export default Body;
