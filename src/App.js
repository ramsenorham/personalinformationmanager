import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './components/header/Header';
import Body from './components/body/Body';
import Footer from './components/footer/Footer';

import './App.css';

function App() {
  return (
    <div className="App body-wrapper">
      <div className="body-content">
        <Router>
          <Header />
          <Body />
        </Router>
      </div>
      <Footer />
    </div>
  );
}

export default App;
