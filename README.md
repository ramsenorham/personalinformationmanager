# PersonalInformationManager



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```bash
cd existing_repo
git remote add origin https://gitlab.com/ramsenorham/personalinformationmanager.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ramsenorham/personalinformationmanager/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

### `installed npm modules`
```bash
npm install react-router-dom
npm install axios
npm install @reduxjs/toolkit
npm install react-redux
npm install @faker-js/faker
npm install tailwindcss -> dann: npx tailwindcss init
```
## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

## PIM

## Description
Projektarbeit mit React:

In einem ersten Schritt soll im Rahmen der Projektarbeit eine PostIt-App "Things To Do" ("Klebezettel" -Hinweis: Post-It® ist eine geschützte Marke von 3M) mit React in einer CRA-Buildumgebung (Create-React-App) erstellt werden. Des Weiteren soll dann die Anwendung um eine Rezeptsuch-App "What can I eat" über eine Rezept-Api (TheMealDB - englischsprachig) erweitert werden. Abschließend soll die im Kurs entwickelte Einkaufsliste "What do I have to buy" mit in die PIM-App eingebunden werden. Optional kann ggf. auch die Produkt-Informationssuche via der API von openfoodfactory aus dem Kurs eingebaut werden.

### 1. PostIt-App:
Bei der PostIt-App "Things To Do" sollen mehrere 2Do-Notiz-Klebezettel ähnlich den bekannten Post-It®-Notes im View dargestellt werden. Hierbei soll lediglich die Desktop-Darstellung, bei der die Notiz-Klebezettel mehrspaltig dargestellt sind, umgesetzt werden. Eine responsive Umsetzung ist explizit nicht erforderlich. Jedoch sollte es bei Umsetzung mit berücksichtigt werden, also die HTML-Struktur so gewählt sein, dass später auch mittels CSS-Media-Queries ein responsives Layout ohne großen Aufwand umsetzbar bleibt. Wir verfolgen jedoch den Desktop-first-Ansatz.
Die einzelnen 2Do-Notiz-Klebezettel sollen aus einem Titel, einer Beschreibung (die eigentlich 2Do-Notiz) sowie den Informationen, wann die Notiz erstellt wurde und wann sie zuletzt geändert wurde, bestehen.
Ferner soll jede Notiz ein UI-Steuerelement besitzen, über das die Notiz als erledigt gekennzeichnet werden kann und eines mit der die Notiz endgültig gelöscht werden kann.
Die 2Do-Notiz-Klebezettel, die als erledigt gekennzeichnet sind, sollen sich in der Darstellung von den aktiven unterscheiden, z. B. mittels Durchstreichen der Inhalte und/oder ausgrauen der Farben.
Der Titel und der Notiz-Inhalt sollen direkt in der Notiz bearbeitbar sein (analog unserer Einkaufsliste oder Seminartabelle in der Seminarverwaltungs-App). Wichtig dabei ist, dass nach Änderungen durch den Benutzer die Information, wann die Notiz zuletzt geändert wurde, aktualisiert wird.
Der Benuzter soll ferner die Möglichkeit haben, eine neue 2Do-Notiz zu erstellen. Die Erstellung sollte über eine Eingabeformular in Form eines Modals erfolgen (vgl. das Hinzufügen eines neuen Items zu unserer Einkaufsliste).
Die Eingabe der Daten einer neue 2Do-Notiz sollen in einfacher Form validiert werden. Kriterium Titel und Beschreibung sind Pflichtfelder.
Zusätzlich soll im View die Möglichkeit bestehen, dass die Notizen gefiltert werden können. Es sollen entweder alle 2Do-Notizen, nur die aktiven (nicht erledigten) oder nur die inaktiven (erledigten) Notizen angezeigt werden können.
Die Reihenfolge der einzelnen 2Do-Notiz-Klebezettel soll ebenfalls auswählbar sein. In der Default-Darstellung sollen sie der Reihenfolge, wie sie aus der Datenquelle gegeben ist, angeordnet werden. Es soll aber auch die Möglichkeit geben, sie nach Erstellungsdatum oder nach Änderungsdatum zu sortieren.

### 2. Datenhaltung und Datenverwaltung:
Die PostIt-App "Things To Do" soll später in einer weiteren Ausbaustufe (nicht in dieser Projektaufgabe!) an ein Backend mit Datenbank-Management-System angebunden werden.
Im Projekt soll diese Anbindung nur simuliert werden. Das bedeutet, dass bestehnde Daten beim Mounten der App-Komponente aus einer Datei eingelesen werden (analog unserem Projekt Seminarverwaltung). Dies soll jedoch asynchron erfolgen, was wir durch einen Timeout von 2 bis 3 Sekunden simulieren.
Es soll eine geeignete Datenstruktur bestimmt werden, also welche Informationen (fields - Felder) muss ein Datensatz enthalten und wie wird er in der Datenbank (allgemein für eine organisierte Sammlung von strukturierten Informationen oder Daten) gehalten.
Die Datenverwaltung erfolgt in der App-Komponente. Hierzu kann React-Redux oder der useReducer-Hook verwendet werden. Das ist aber keine Anforderungen, es kann auch mit den React-Bordmittel z. B. mittels entsprechender state-Variable(n) erfolgen.

### 3. Rezeptsuch-App - "What can I eat":
Die App soll in einer ersten Ausbaustufe um eine Rezeptsuch-App "What can I eat" erweitert werden. Hierzu kann die englischsprachig Rezept-Api (TheMealDB) genutzt werden, bei der es einen Test-Api-Key ('You can use the test API key "1" during development of your app or for educational use...') gibt, verwendet werden.
Die Rezeptsuch-App soll aus einen Such-Eingabe-Feld bestehen, bei der man auf Englisch einen Suchbegriff eingeben kann (z. B. "chicken"), mit dem dann bei der Api um Rezepte angefragt wird.
Die Ergebnistreffer sollen in Form einer Liste jeweils mit dem Rezept-Titel, Rezept-Kategorie, Rezept-Tags, einem Rezept-Vorschaubild (sofern vorhanden) so wie einem Link zu einem Youtube-Video dargestellt werden. Der Link zum Video soll dabei in einem neuen Browser-Tab bzw. Browser-Fenster öffnen.
Falls kein Link zu einem Youtube-Video vorhanden ist, soll auf eine Suchmaschinen-Seite mit Suche nach zumindest dem Begriff (z. B. 'https://www.google.com/search?q=youtube+rezept+chicken') verlinkt werden.
Falls kein Rezept gefunden wird, soll ein entsprechender Hinweis erfolgen.
Das Suchfeld soll auch bei Anzeige der Trefferliste im View verbleiben, so dass der Benutzer auch direkt weitere Suchen ausführen kann.
Das Design und Layout der Liste ist individuell frei wählbar.

### 4. PIM-App - Navigation:
Die beiden App-Komponenten (Komponente hier im Sinne Komponenten nach UML) PostIt-App und Rezeptsuch-App sollen in der PIM-App über ein Navigationsmenü eingebunden werden. Die Einbindung soll über den React-Router erfolgen und die einzelnen App-Komponenten mittels NavLinks verlinkt werden.
Das Design und Layout der PIM-App ist individuell frei wählbar.

### 5. Einbinden der Einkaufsliste-App - "What do I have to buy":
Im der letzten Ausbaustufe innerhalb der Projektarbeit soll die Einkaufsliste-App aus dem Kurs in die PIMApp eingebunden werden.
Das Design der Einkaufsliste-App muss nicht an das Design der PIM-App angepasst werden. Es ist jedoch darauf zu achten, dass sich die Style-Angaben der einzelnen App-Komponenten nicht gegenseitig beeinflussen und zu Fehldarstellungen führen.
Für die Einkaufsliste soll die Datenhaltung und Datenverwaltung analog der PostIt-App (s. a. Datenhaltung und Datenverwaltung) erfolgen.
Eine Daten-Abfrage beim Backend-Server wie im Kurs umgesetzt soll nicht erfolgen.

## Project Requirement

### 1. PostIt-App "Things To Do"
•	Desktop-Ansicht mit mehrspaltiger Darstellung von 2Do-Notiz-Klebezetteln.

•	Notiz-Klebezettel bestehen aus Titel, Beschreibung, Erstellungsdatum und Änderungsdatum.

•	UI-Steuerelemente für Markierung als erledigt und endgültiges Löschen.

•	Unterscheidung der Darstellung von erledigten und aktiven Notizen.

•	Bearbeitbare Titel und Notiz-Inhalte mit Aktualisierung der Änderungsinformation.

•	Modalformular zur Erstellung neuer 2Do-Notizen mit einfacher Validierung.

•	Filtermöglichkeit für alle, aktive und inaktive Notizen.

•	Sortierung nach Erstellungsdatum oder Änderungsdatum.

### 2. Datenhaltung und Datenverwaltung
•	Simulierte Anbindung an ein Backend durch asynchrones Einlesen von Daten aus einer Datei (Timeout von 2-3 Sekunden).

•	Festlegung geeigneter Datenstruktur für die 2Do-Notizen.

•	Datenverwaltung in der App-Komponente, optional mit React-Redux oder useReducer-Hook.

### 3. Rezeptsuch-App "What can I eat"
•	Integration einer Rezeptsuch-App mit Nutzung der TheMealDB-Rezept-API.

•	Suchfeld für Eingabe von Suchbegriffen auf Englisch.

•	Anzeige von Rezepten mit Titel, Kategorie, Tags, Vorschaubild und Link zu Youtube-Video oder Suchmaschinen-Seite.

•	Hinweis bei fehlenden Rezepten.

•	Suchfeld bleibt im View sichtbar.

•	Individuell wählbares Design und Layout für die Ergebnisliste.

### 4. PIM-App - Navigation
•	Einbindung von PostIt-App und Rezeptsuch-App in eine PIM-App.

•	Verwendung von React-Router und NavLinks für die Navigation.

•	Individuell wählbares Design und Layout für die PIM-App.

### 5. Einbinden der Einkaufsliste-App
•	Einbindung der Einkaufsliste-App in die PIM-App.

•	Unabhängiges Design der Einkaufsliste-App, ohne Beeinflussung der Style-Angaben der anderen App-Komponenten.

•	Datenhaltung und Datenverwaltung analog zur PostIt-App, ohne Backend-Abfrage.

## Request Status

|Nr.| Request | Description | status |
| - | -------- | -------- | -------- |
|1| Plan erstellen  | Eine einfache Komponenteplan erstellen | ok |
|2| PostIt-App |||
||| eigene HTML, CSS (bootstrapt) templat erstellen und in react umgebung integrieren | ok |
||| Desktop-Ansicht mit mehrspaltiger Darstellung von 2Do-Notiz-Klebezetteln | ok |
||| Notiz-Klebezettel bestehen aus Titel, Beschreibung, Erstellungsdatum und Änderungsdatum | ok |
||| UI-Steuerelemente für Markierung als erledigt | ok |
||| Die erledigte endgültiges Löschen | ok |
||| Unterscheidung der Darstellung von erledigten und aktiven Notizen | ok |
||| Bearbeitbare Titel und Notiz-Inhalte mit Aktualisierung der Änderungsinformation | ... |
||| Modalformular zur Erstellung neuer 2Do-Notizen mit einfacher Validierung | ok |
||| Filtermöglichkeit für alle, aktive und inaktive Notizen | ok |
||| Sortierung nach Erstellungsdatum oder Änderungsdatum | ok |
|3| Datenhaltung und Datenverwaltung |||
||| Simulierte Anbindung an ein Backend durch asynchrones Einlesen von Daten aus einer Datei (Timeout von 2-3 Sekunden) | ... |
||| Festlegung geeigneter Datenstruktur für die 2Do-Notizen | ok |
||| Datenverwaltung in der App-Komponente, optional mit React-Redux oder useReducer-Hook | ok |
|4| Rezeptsuch-App |||
||| Integration einer Rezeptsuch-App mit Nutzung der TheMealDB-Rezept-API | ok |
||| Suchfeld für Eingabe von Suchbegriffen auf Englisch | ok |
||| Anzeige von Rezepten mit Titel, Kategorie, Tags, Vorschaubild und Link zu Youtube-Video oder Suchmaschinen-Seite | ok |
||| Hinweis bei fehlenden Rezepten | ok |
||| Suchfeld bleibt im View sichtbar | ok |
||| Individuell wählbares Design und Layout für die Ergebnisliste | ok |
|5| PIM-App - Navigation |||
||| Einbindung von PostIt-App und Rezeptsuch-App in eine PIM-App | ok |
||| Verwendung von React-Router und NavLinks für die Navigation | ok |
||| Individuell wählbares Design und Layout für die PIM-App | ok |
|6| Einbinden der Einkaufsliste-App |||
||| Einbindung der Einkaufsliste-App in die PIM-App | ok |
||| Unabhängiges Design der Einkaufsliste-App, ohne Beeinflussung der Style-Angaben der anderen App-Komponenten | ok |
||| Datenhaltung und Datenverwaltung analog zur PostIt-App, ohne Backend-Abfrage | ok |
|7| Dokumentation | Das Projekt in Gitlap integrieren und commiten. | ok |

## PIM-APP Komponente Plan:

skizze Link: https://excalidraw.com/#json=IgBwrMc5HAAgMCkJGzwqs,HpvHpFtAHKiIn9KLOovapA

![Komponente Plan](./documentation/PIM_App.png)

## PIM-APP Page Form:

skizze Link: https://excalidraw.com/#json=yvb6bLZT8qISsdwtEDOAT,cDU987PKYxfHzOz7L9q5Vg

![Page Form](./documentation/PIM_Page.png)

## Beispiele für such Begriffe 
Arrabiata

Lamb Pilaf (Plov)

in API https://www.themealdb.com/api.php:

API: https://www.themealdb.com/

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below.
Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.
